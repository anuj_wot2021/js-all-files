// this functions works for next image

function nextImage() {
    var activeImage = document.getElementsByClassName('slider-images active')[0];
    var totalSlides = document.getElementsByClassName('slider-images').length;
    var currentDots = document.getElementsByClassName('dot show')[0];
    var currentIndex = Array.prototype.indexOf.call(activeImage.parentNode.children, activeImage,currentDots);
    if(currentIndex+1 == totalSlides) {
        document.getElementsByClassName('slider-images')[0].classList.add("active");
        document.getElementsByClassName('dot')[0].classList.add('show')
    }else{
        activeImage.nextElementSibling.classList.add("active");
        currentDots.nextElementSibling.classList.add('show');
    }    
    activeImage.classList.remove("active");    
    currentDots.classList.remove('show');

//   this function will work for previous image 

}
function prevImage(){
    var activeImage = document.getElementsByClassName('slider-images active')[0];
    var totalSlides = document.getElementsByClassName('slider-images').length;
    var currentDots = document.getElementsByClassName('dot show')[0];
    var currentIndex = Array.prototype.indexOf.call(activeImage.parentNode.children, activeImage,currentDots);
    if(currentIndex == 0) {
        document.getElementsByClassName('slider-images')[totalSlides-1].classList.add("active")
        document.getElementsByClassName('dot')[totalSlides-1].classList.add('show')
    }else{
        activeImage.previousElementSibling.classList.add("active");
        currentDots.previousElementSibling.classList.add('show');
    }    
    activeImage.classList.remove("active"); 
    currentDots.classList.remove('show');

}
// to run the dots 
function myDotFunction(e){
    var currentIndex = Array.prototype.indexOf.call(e.target.parentNode.children, e.target,e.target);
    document.getElementsByClassName('slider-images active')[0].classList.remove("active")
    document.getElementsByClassName('dot show')[0].classList.remove('show');
    document.getElementsByClassName('slider-images')[currentIndex].classList.add("active")
    document.getElementsByClassName('dot')[currentIndex].classList.add('show')

   
}
// this is to start the slider and disable the slider-on button of once click
var autoMatic;
function start(){
    autoMatic = setInterval(nextImage, 2000);
    document.getElementById("start").disabled = true;

}

// this is to stop the auto-slide show
function stop(){
    clearInterval(autoMatic);
    document.getElementById("start").disabled = false;
}